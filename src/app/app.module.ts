import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { CollectionsComponent } from './collections/collections.component';
import { ServiceComponent } from './service/service.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { FragranceComponent } from './home/fragrance/fragrance.component';
import { BestSellerComponent } from './home/best-seller/best-seller.component';
import { NewArrivalsComponent } from './collections/new-arrivals/new-arrivals.component';
import { TopPicksComponent } from './collections/top-picks/top-picks.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    CollectionsComponent,
    ServiceComponent,
    LoginComponent,
    FragranceComponent,
    BestSellerComponent,
    NewArrivalsComponent,
    TopPicksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
