import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  defaultQuestion="pet";
  @ViewChild('f') signupForm:NgForm | undefined;
  answer="";
  genders=["male", "female"];
  submitted=false;
  user={
    username:'',
    email:'',
    password:'',
    secret:'',
    questionAnswer:'',
    gender:''
  }
  constructor() { }

  ngOnInit(): void {
  }
  
  
  // onSubmit(form:NgForm){
  //   console.log(form);
  // }wecan use another method where without passing parameter(method*)
  
  onSubmit()
  {
    this.submitted=true;
  this.user.username = this.signupForm?.value.userData.name;
  this.user.email = this.signupForm?.value.userData.email;
  this.user.password = this.signupForm?.value.userData.password;
  this.user.secret = this.signupForm?.value.userData.secret;
  this.user.questionAnswer = this.signupForm?.value.userData.questionAnswer;
  this.user.gender = this.signupForm?.value.userData.gender;
  this.signupForm?.reset();
  
  
  }
    
  

}
